from sanic import exceptions as excep


class SanicJWTException(excep.SanicException):
    pass


class AuthenticationFailed(excep.Unauthorized):
    def __init__(self, message="Authentication failed."):
        super().__init__(message)


class MissingAuthorizationHeader(excep.InvalidUsage):
    def __init__(self, message="Authorization header not present."):
        super().__init__(message)


class MissingAuthorizationCookie(excep.InvalidUsage):
    def __init__(self, message="Authorization cookie not present."):
        super().__init__(message)


class InvalidAuthorizationHeader(excep.InvalidUsage):
    def __init__(self, message="Authorization header is invalid."):
        super().__init__(message)


class RefreshTokenNotImplemented(excep.ServerError):
    def __init__(self, message="Refresh tokens have not been enabled."):
        super().__init__(message)


class MissingRegisteredClaim(excep.ServerError):
    def __init__(self, message="One or more claims have been registered, but your SANIC_JWT_HANDLER_PAYLOAD_EXTEND does not supply them. ", missing=None):
        if missing:
            message += str(missing)
        super().__init__(message)


class MeEndpointNotSetup(excep.ServerError):
    def __init__(self, message="/me endpoint has not been setup. Pass retrieve_user if you with to proceeed."):
        super().__init__(message)


class Unauthorized(SanicJWTException, excep.Unauthorized):
    def __init__(self):
        super().__init__("Auth required.", scheme="Bearer")


class InvalidClassViewsFormat(SanicJWTException):
    def __init__(self, message="class_views should follow this format ('<SOME ROUTE>', ClassInheritedFromHTTPMethodView)"):
        super().__init__(message)